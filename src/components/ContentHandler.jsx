import React, { useState, useRef, useEffect } from "react";
import { useSwipeable, Swipeable } from "react-swipeable";

import Section from "./Section";
import { css } from "emotion";
import classNames from "classnames";
import DrawLogo from "./DrawLogo";
import MenuHandler from "./MenuHandler";

const ContentHandler = () => {
  const [vPSize, setVPSize] = useState(10);
  const [count, setCount] = useState(0);
  useEffect(() => {
    window.addEventListener("scroll", eventObj);
    const interval = setInterval(() => {
      count >= listImages.length - 1 ? setCount(0) : setCount(count + 1);
      changeBg("PREV");
    }, 12000);
    return () => clearInterval(interval);
  });

  const changeBg = (val) => {
    let newListImages = [...listImages];
    newListImages.map((item, index) => {
      if (val != null && val == "NEXT") {
        count >= listImages.length ? setCount(0) : setCount(count + 1);
        item.opacity = 0;
        item.xPos = "0%";
        if (index == count) {
          item.opacity = 0;
          item.xPos = "-30%";
        } else if (
          index == count + 1 ||
          (count + 1 > listImages.length - 1 && index == 0)
        ) {
          item.opacity = 1;
          item.xPos = "0%";
        }
      } else if (val != null && val == "PREV") {
        count <= 0 ? setCount(listImages.length - 1) : setCount(count - 1);
        item.opacity = 0;
        item.xPos = "0%";
        if (index == count) {
          item.opacity = 0;
          item.xPos = "30%";
        } else if (
          index == count - 1 ||
          (count <= 0 && index == listImages.length - 1)
        ) {
          item.opacity = 1;
          item.xPos = "0%";
        }
      }
    });

    setListImages(newListImages);
  };

  const eventObj = {
    handleEvent(e) {
      setVPSize(window.pageYOffset);
    },
  };

  const [listImages, setListImages] = useState([
    { url: "../img/bg01.jpg", opacity: 1, xPos: "0vw" },
    { url: "../img/bg06.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg02.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg07.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg03.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg08.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg04.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg09.jpg", opacity: 0, xPos: "0vw" },
    { url: "../img/bg05.jpg", opacity: 0, xPos: "0vw" },
  ]);

  const handlers = useSwipeable({
    onSwipedLeft: () => slide("NEXT"),
    onSwipedRight: () => slide("PREV"),
    preventDefaultTouchmoveEvent: true,
    trackMouse: true,
  });
  const slide = (val) => {
    if (val == "NEXT") {
      changeBg(val);
    } else {
      changeBg(val);
    }
  };

  const spacer = css`
    opacity: ${0.5 - vPSize / 800};
  `;
  const spacerDist = css`
    opacity: 0;
    height: 40vh;
  `;

  return (
    <div id="opening" className="main-container" {...handlers}>
      <MenuHandler offset={vPSize} />
      {listImages.map((item, index) => (
        <div
          className={classNames(
            "background-gallery",
            css`
              background-image: url(${item.url});
              opacity: ${item.opacity};
              transform: translateX(${item.xPos});
            `
          )}   
                 
        >  <DrawLogo hasAnim="true" /></div>
        
      ))}
      <div className="opening-container">
        <div className="gallery-position"></div>
      
        <div className="gallery-position">
          {listImages.map((item, index) => (
            <div
              className={classNames(
                "gallery-position__item",
                css`
                  background-color: ${item.opacity > 0 ? 'var(--color-light)' : 'var(--color-a)'};
                `
              )}
            >
              &nbsp;
            </div>
          ))}
        </div>
      </div>

      <div className={classNames("spacer-v", spacer)}> </div>

      <div id="nextEvent" className="section-container">
        <Section
          contentId="nextEvent"
          title="EVENT A VENIR"
          titleColor="var(--color-d)"
          description="Nous invitons des amis chez nous pour partager un espace où la gastronomie, comme moyen d'union, nous unit autour d'un plat préparé avec amour et dévouement."
        />
      </div>
      <div id="past" className="section-container">
        <Section
          contentId="pastEvents"
          title="déjà fait"
          titleColor="var(--color-d)"
          description="Nous avons préparé des dîners informels mais spéciaux pour les amis de la maison, avec qui nous avons passé des moments qui restent toujours dans nos bons souvenirs."
        />
      </div>

      <div className="message">
        <span
          data-aos="zoom-in-up"
          className="message__content message__content-l"
        >
          on aime
        </span>
        <span
          data-aos="zoom-in-down"
          className="message__content message__content-r"
        >
          et on fait
        </span>
      </div>

      <div id="contact" className="section-container">
        <Section
          contentId="contact"
          title="CONTACT"
          titleColor="var(--color-d)"
          description="Nous voulons avoir de vos nouvelles, n'hésitez pas à nous le faire savoir."
        />
      </div>
      <footer className="footer">
        <a href="mailto:ricardo.santoyo@hotmail.com" className="footer__a">
          By Ricardo Santoyo
        </a>
      </footer>
    </div>
  );
};

export default ContentHandler;
