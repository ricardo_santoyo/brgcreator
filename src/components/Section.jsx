import React, { useState, useRef, useEffect } from "react";
import detectIt from "detect-it";
import { css } from "emotion";
import classNames from "classnames";
import ProductList from "./ProductList";

const Section = (props) => {
  const [vPSize, setVPSize] = useState(10);
  useEffect(async () => {
    window.addEventListener("scroll", eventObj);
  }, []);
  const eventObj = {
    handleEvent(e) {
      setVPSize(window.pageYOffset);
    },
  };
  const logo__sub = css`
    font-weight: 600;
    font-size: 2.5rem;
    width: 85%;
    color: ${props.titleColor};
    &::before,
    &::after {
      background-color: ${props.titleColor};
    }
  `;
  const section__header = css`
    background-color: ${props.bgColor};
  `;

  const mySwitchFunction = (param) => {
    switch (param) {


      case "contact":
        return [
          <div className="contact">
            <div className="social">
              <a
                href="https://www.instagram.com/chezricard.ca/"
                className="social__a"
              >
                <i class="fab fa-instagram"></i>
              </a>
              <a
                href="https://www.facebook.com/chezricard"
                className="social__a"
              >
                <i class="fab fa-facebook-f"></i>
              </a>
              <a
                href="mailto:ricardo.santoyo@hotmail.com"
                className="social__a"
              >
                <i class="fas fa-at"></i>
              </a>
            </div>
          </div>,
        ];
      default://full production list according to the id
        return [<ProductList contentId={props.contentId} />];
    }
  };

  return (
    <section className="section">
      <header className={classNames("section__header", section__header)}>
        <div className={classNames("logo__sub", logo__sub)}>{props.title}</div>
      </header>
      {props.description ? (
        <p className="section__description">{props.description}</p>
      ) : (
        <div></div>
      )}

      {
        props.contentId != null ? mySwitchFunction(props.contentId) : <div />

        // (props.contentId != null) ?
        //     <ProductList contentId={props.contentId}/>  :

        //     <form action="" className="upcoming-event">
        //         <img  src="../img/zoomevent.jpg" alt="" className="upcoming-event__img"/>
        //         <div   className="upcoming-event__info">
        //             <div className="upcoming-event__info-title">ZOOM DEJAUNEUR</div>
        //             <div className="upcoming-event__info-date">Samedi, Mai 23, 2020</div>
        //         </div>

        //     </form>
      }
    </section>
  );
};

export default Section;
