import React, { useState, useRef, useEffect, useCallback } from "react";
import { css } from "emotion";
import classNames from "classnames";

const DrawLogo = (props) => {
  const [vPSize, setVPSize] = useState(10);
  const [messages, setMessages] = useState([
    { text: "Ca va bien aller" },
    { text: "A Cozy Winter" },
    { text: "L'amour est cuit a feux doux" },
    { text: "profitez de la vie" },
  ]);
  const [message, setMessage] = useState(
    messages[Math.floor(Math.random() * messages.length)].text
  );
  useEffect(() => {
    window.addEventListener("scroll", eventObj);
  }, []);
  const eventObj = {
    handleEvent(e) {
      setVPSize(window.pageYOffset);
    },
  };

  const logo = css`
    opacity: ${1 - vPSize / 230};
    transform: scale(${1 + 0.005 * vPSize});
  `;

  return (
    // <div  className={classNames("logo", logo)} >
    <div
      className={props.hasAnim == "true" ? classNames("logo", logo) : "logo"}
    >
      <div className={classNames("logo__head")}>Chez Ricard</div>
      {!props.hideSub ? (
        <div className={classNames("logo__sub")}>{message}</div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default DrawLogo;
