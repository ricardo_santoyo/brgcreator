import React, { useState, useRef, useEffect } from "react";
import detectIt from "detect-it";
import { css } from "emotion";
//import classNames from 'classnames'

const ProductList = (props) => {
  const [vPSize, setVPSize] = useState(10);

  const [burgersList, setBurgersList] = useState([
    {
      title: "ZOOM EVENT",
      date: "Pour Confirmer",
      instagram: "https://www.instagram.com/p/Btwz8NFAZ6T/",
      description:
        "Nous vous attendons lors de notre prochain événement via zoom",
      img: "../img/zoomevent.jpg",
    },
  ]);
  const [eventsList, setEventsList] = useState([
    {
      title: "Pannecook",
      date: "2020/01/01",
      instagram: "https://www.instagram.com/p/Btwz8NFAZ6T/",
      description:
        "pain fait maison, garni de veau au champignon, accompagné d'une soupe à l'oignon.",
      img: "../img/bg4.jpg",
    },
    {
      title: "saumon",
      date: "2020/01/01",
      instagram: "",
      description:
        "Cuisiner à l'extérieur est un énorme plaisir, car la maîtrise du feu devient la principale source de saveur, fournissant une saveur rustique, mais inégalée.",
      img: "../img/saumon.jpg",
    },
    {
      title: "Printemps Burger",
      date: "2020/01/01",
      instagram: "",
      description:
        "La mangue, le paprika et le fromage à la crème font de ce hamburger une excellente option pour les jours joyeux de l'été.",
      img: "../img/spring.jpg",
    },
    {
      title: "pizza",
      date: "2020/01/01",
      instagram: "",
      description:
        "Pâte de levure maison et sauce tomate cerise basilic, parfaite pour une soirée avec un vin rouge",
      img: "../img/pizza.jpg",
    },
  ]);

  useEffect(() => {
    window.addEventListener("scroll", eventObj);
  }, []);
  const eventObj = {
    handleEvent(e) {
      setVPSize(window.pageYOffset);
    },
  };

  const selectContent = (contentType) => {
    switch (contentType) {
      case "pastEvents":
        return eventsList;
        break;
      case "nextEvent":
        return burgersList;
        break;

      default:
        return eventsList;
    }
  };

  const logo__sub = css`            
        //   opacity: ${vPSize / 210 > 1 ? vPSize / 580 : 0};        
        //   transform: scale(${vPSize / 500 > 0.8 ? 0.8 : vPSize / 500});
           font-weight:  600;
          //font-size: ${vPSize / 10 > 5 ? 5 : vPSize / 10}rem;
          font-size: 2.5rem;
          //transform: scale(0.9);
          width: 85%;
    `;
  const spacer = css`
    opacity: ${0.5 - vPSize / 800};
  `;
  return (
    <main className="section__main">
      <div className="products">
        {selectContent(props.contentId).map((item, index) => (
          <div data-aos="zoom-in-up" className="product-card">
            <img src={item.img} alt="" className="product-card__img" />
            <div className="product-card__info">
              <div className="product-card__title">{item.title}</div>

              <div className="product-card__date">{item.date}</div>
              <div className="product-card__description">
                {item.description}
              </div>
            </div>
          </div>
        ))}
      </div>
    </main>
  );
};

export default ProductList;
