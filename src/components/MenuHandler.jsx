import React, { useState, useRef } from "react";
import detectIt from "detect-it";
import ThemesManager from "./ThemesManager";
import DrawLogo from './DrawLogo';

const MenuHandler = (props) => {
  const [hovered, setHovered] = useState(false); 
  const checkBoxEl = useRef(null);
  const themeManager = useRef(null);
  const [menuData, setMenuData] = useState([
    { text: "accueil", link: "#opening", img: "../img/bg1.jpg" },
    { text: "a venir", link: "#nextEvent", img: "../img/bg2.jpg" },
    { text: "déjà fait", link: "#past", img: "../img/bg3.jpg" },
    { text: "contact", link: "#contact", img: "../img/bg4.jpg" },
  ]);

  const handleClick = (e) => {
    if (checkBoxEl.current != null) {
      checkBoxEl.current.checked = !checkBoxEl.current.checked;
    }
  };
  const handleOver = (e) => {
    setHovered(detectIt.hasMouse);
  };
  const handleOut = (e) => {
    setHovered(false);
  };

  const [themePicker, setThemePicker] = useState(["red", "yellow", "green",  "dark"]);
  const [selectedTheme, setSelectedTheme] = useState("red");
  const [indexTheme, setIndexTheme] = useState(1);
  const updateTheme = (e) => {
    setSelectedTheme(themePicker[indexTheme]);
    setIndexTheme(indexTheme + 1);
    if (indexTheme > 2) {
      setIndexTheme(0);
    }
  };

  return (
    <div className="nav-container">
      <ThemesManager ref={themeManager} theme={selectedTheme} />
      {props.offset > 50 ? (
        <div className="navigation">
          <input
            ref={checkBoxEl}
            type="checkbox"
            className="navigation__checkbox"
            id="navi-toggle"
          />
          <label
            for="navi-toggle"
            className="navigation__button"
            onMouseOver={handleOver}
            onMouseLeave={handleOut}
          >
            <i
              class="navigation__icon-on fas fa-hamburger"
              aria-hidden="true"
            ></i>
            <i class="navigation__icon-off fas fa-times" aria-hidden="true"></i>
          </label>
          <div
            className={
              hovered
                ? "navigation__background navigation__background-hovered"
                : "navigation__background "
            }
          >&nbsp;
          </div>
          
          <nav className="navigation__nav">
            
         
            <DrawLogo/>
            <div className="navigation__themify">
              <i className="navigation__themify-i fas fa-palette" onClick={updateTheme}></i>
              <i className="navigation__themify-i fas fa-palette" onClick={updateTheme}></i>
            </div>
            <ul className="navigation__list">
              
              {menuData.map((item, index) => (
                <li onClick={handleClick} className="navigation__item">
                  <div
                    className="navigation__item-img"
                      style={{ backgroundImage: `url(${item.img})` }}>
                  </div>
                  <a href={item.link} className="navigation__link">
                    <span className="navigation__link-span" >{item.text}</span>
                  </a>
                </li>
              ))}
            </ul>
          </nav>
        </div>
      ) : (
        <div></div>
      )}

      <div className="nav-bar">
        <div className="nav-bar__themify">
          <i className="nav-bar__themify-picker fas fa-palette" onClick={updateTheme}></i>
        </div>
        <ul className="nav-bar__ul">
          {menuData.map((item, index) => (
            <li onClick={handleClick} className="nav-bar__li">
              <a href={item.link} className="nav-bar__a">
                <span>{item.text}</span>
              </a>
            </li>
          ))}
        </ul>
        <div className="nav-bar__themify">
          <i className="nav-bar__themify-picker fas fa-palette" onClick={updateTheme}></i>
        </div>
      </div>
    </div>
  );
};

export default MenuHandler;
