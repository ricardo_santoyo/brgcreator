import React, { useState, useRef, useEffect } from "react";

const ThemesManager = (props, ref) => {
  const [colors, setColors] = useState({
    red: "rgb(211, 38, 44)",
    redFilter: "rgba(211, 38, 44, 0.3)",
    green: "rgb(43, 66, 12)",
    greenFilter: "rgba(43, 66, 12, 0.3)",
    yellow: "rgb(245, 191, 75)",
    yellowFilter: "rgba(245, 191, 75, 0.3)",
    lemon: "rgb(197, 188, 64)",
    brown: "rgb(185, 101, 56)",
    light: "rgb(247, 246, 235)",
    dark: "rgb(6, 8, 4)",
  });
  const [theme, setTheme] = useState({
    red: {
      a: colors.red,
      b: colors.green,
      c: colors.yellow,
      c_alpha: colors.yellowFilter,
      d: colors.light,
      e: colors.yellow, //next color palette
      light: colors.light,
    },   

    yellow: {
      a: colors.yellow,
      b: colors.red,
      c: colors.green,
      c_alpha: colors.greenFilter,
      d: colors.dark,
      e: colors.green,
      light: colors.light,
    },
    green: {
      a: colors.green,
      b: colors.red,
      c: colors.yellow,
      c_alpha: colors.redFilter,
      d: colors.light,
      e: colors.dark,
      light: colors.light,
    },
    dark: {
      a: colors.dark,
      b: colors.red,
      c: colors.yellow,
      c_alpha: colors.greenFilter,
      d: colors.brown,
      e: colors.yellow, //next color palette
      light: colors.light,
    },
  });
  const [selectedTheme, setSelectedTheme] = useState(theme.yellow);

  useEffect(() => updateTheme());

  const updateTheme = () => {
    switch (props.theme) {
      case "green":
        setSelectedTheme(theme.green);
        break;
      case "yellow":
        setSelectedTheme(theme.yellow);
        break;
      case "dark":
        setSelectedTheme(theme.dark);
        break;
      default:
        setSelectedTheme(theme.red);
    }
    Object.keys(selectedTheme).forEach((key) => {
      const themeKey = `--color-${key}`;
      const themeValue = selectedTheme[key];
      document.body.style.setProperty(themeKey, themeValue);
    });
  };

  return <div></div>;
};

export default ThemesManager;
