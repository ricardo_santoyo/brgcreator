import React, { Component } from "react";
import MenuHandler from "./components/MenuHandler";
import AOS from "aos";
import "aos/dist/aos.css";
import "./sass/main.css";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import { Container } from "@material-ui/core";
import ProductList from "./components/ProductList";
import DrawLogo from "./components/DrawLogo";
import Section from "./components/Section";
import { css } from "emotion";

import ContentHandler from "./components/ContentHandler";

class App extends Component {
  componentDidMount() {
    AOS.init();
  }

  render() {
    return (
      <div className="app-container">
        <ContentHandler />
      </div>
    );
  }
}

export default App;
